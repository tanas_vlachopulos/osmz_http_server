package com.example.tanas.httpserver2;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import layout.ServerNotification;

public class SocketServerService extends Service {

	private final IBinder mBinder = new SocketServerServiceBinder();
	private boolean isRunning = false;
	private boolean isBounded = false;
	private SocketServer s;

	CameraImageHolder imageHolder;
	HandlerHolder handlerHolder;

	private ResultReceiver resultReceiver = null;
	private BroadcastReceiver br;

	// handle log messages from client threads
	private Handler h = new Handler(Looper.getMainLooper()) {
		@Override
		public void handleMessage(Message inputMessage) {
			Bundle msg = inputMessage.getData();

			Log.d("SERVER", "Message from thread: " + msg.get("client") + " " + msg.get("file"));

			// send log info to UI thread
			if (resultReceiver != null && isBounded) {
				resultReceiver.send(200, msg);
			}
		}
	};

	public SocketServerService() {
	}

	@Override
	public void onCreate() {
		imageHolder = new CameraImageHolder();
		handlerHolder = new HandlerHolder();

		br = new NotificationReceiver();
		IntentFilter filter = new IntentFilter("com.example.tanas.NOTIFY_BROADCAST");
		this.registerReceiver(br, filter);

		Log.d("SERVER", "Service start");
	}

	// start service job
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("SERVER", "Service start command - activity is bounded? " + String.valueOf(isBounded));

		// obtain result receiver from UI activity for sending connection log
		if (intent != null) {
			resultReceiver = intent.getParcelableExtra("results");
		}

		if (!isRunning) {
//			testOutput();

			s = new SocketServer(h, imageHolder, handlerHolder);
			s.start();

			ServerNotification.notify(this, "Http Server", 1);

			isRunning = true;
		}

		return Service.START_STICKY;
	}

	public CameraImageHolder getImageHolder() {
		return this.imageHolder;
	}

	public HandlerHolder getHandlerHolder() {
		return this.handlerHolder;
	}

	// output for service testing
	private void testOutput() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < 60; i++) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					Log.d("SERVER", "Service tick " + String.valueOf(i));

				}

				stopSelf();
			}
		}).start();
	}

	private void sendMessageToMainActivity(String topic, String message) {
		if (resultReceiver != null && isBounded) {
			Bundle bundle = new Bundle();
			bundle.putString(topic, message);
			resultReceiver.send(200, bundle);
		}
	}

	// bind service to activity
	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		Log.d("SERVER", "Binding service ");
		isBounded = true;
		return mBinder;
	}

	// unbind service from activity
	@Override
	public boolean onUnbind(Intent intent) {
		Log.d("SERVER", "Unbind service");
		isBounded = false;
		return true;
	}

	// this service cannot be destroyed
	@Override
	public void onDestroy() {
//		isRunning = false;\
		this.unregisterReceiver(br);
		Log.d("SERVER", "Destroying service");
	}

	// provide binding instance to activity
	public class SocketServerServiceBinder extends Binder {
		SocketServerService getService() {
			return SocketServerService.this;
		}
	}

	private class NotificationReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			boolean startAction = intent.getBooleanExtra("startAction", false);
			if (startAction) {
				if (!isRunning) {
					s = new SocketServer(h, imageHolder, handlerHolder);
					s.start();
					isRunning = true;
					sendMessageToMainActivity("serverStatus", "RUNNING");
				}
			} else {
				s.close();
				try {
					s.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					isRunning = false;
					sendMessageToMainActivity("serverStatus", "NOT RUNNING");
				}
			}
			Log.d("SERVER", "Broadcast received");

		}
	}

}

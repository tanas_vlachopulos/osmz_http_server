package com.example.tanas.httpserver2;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;


public class SocketServer extends Thread {
	ServerSocket serverSocket;
	public final int port = 12345;
	boolean bRunning;
	Handler uiHandler;
	CameraImageHolder imageHolder;
	HandlerHolder cameraHandlerHolder;

	Semaphore semaphore;

	public SocketServer(Handler uiHandler, CameraImageHolder imageHolder, HandlerHolder cameraHandlerHolder) {
		this.uiHandler = uiHandler;
		this.semaphore = new Semaphore(4);
		this.imageHolder = imageHolder;
		this.cameraHandlerHolder = cameraHandlerHolder;
	}

	public void close() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			Log.d("SERVER", "Error, probably interrupted in accept(), see log");
			e.printStackTrace();
		} catch (NullPointerException e) {
			Log.d("SERVER", "Server is already close");
		}
		bRunning = false;
	}

	private void sendError503(Socket s) {
		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
			out.write("HTTP/1.x 200 OK\n");
			out.write("Server: LiteSpeed\n");
			out.write("Content-Type: text/html; charset=UTF-8\n");
			out.write("\n");
			out.write("<h1>Server overloaded</h1>");
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Log.d("SERVER", "Error 503 service overloaded");
	}

	public void run() {
		try {
			Log.d("SERVER", "Creating Socket");
			serverSocket = new ServerSocket(port);
			bRunning = true;


			while (bRunning) {
				Log.d("SERVER", "Socket Waiting for connection");
				Socket s = serverSocket.accept();
				Log.d("SERVER", "Socket Accepted");

				if (semaphore.tryAcquire()) {

					ServerClient clientHandle = new ServerClient(s, uiHandler, semaphore, imageHolder, cameraHandlerHolder);
					clientHandle.start();
				}
				else {
					sendError503(s);
					s.close();
				}
				Log.d("SERVER", String.valueOf(semaphore.availablePermits()));
			}
		} catch (IOException e) {
			if (serverSocket != null && serverSocket.isClosed())
				Log.d("SERVER", "Normal exit");
			else {
				Log.d("SERVER", "Error");
//				e.printStackTrace();
			}
		} finally {
			serverSocket = null;
			bRunning = false;
		}
	}
}

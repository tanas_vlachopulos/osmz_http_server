package com.example.tanas.httpserver2;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.util.ArrayList;
import java.util.List;

public class HandlerHolder {

	private ArrayList<Handler> handlerList;

	public HandlerHolder() {
		handlerList = new ArrayList<>();
	}

	public void addHandler(Handler handler) {
		this.handlerList.add(handler);
	}

	public void removeHandler(Handler handler) {
		this.handlerList.remove(handler);
	}

	public void sendDataToAllHanders(byte[] data) {

		if (handlerList != null) {
			for (Handler h : handlerList) {
				Message msg = new Message();
				Bundle bundle = new Bundle();
				bundle.putByteArray("data", data);
				msg.setData(bundle);

				h.sendMessage(msg);
			}
		}
	}
}

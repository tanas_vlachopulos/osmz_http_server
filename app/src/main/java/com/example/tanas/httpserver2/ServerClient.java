package com.example.tanas.httpserver2;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerClient extends Thread {
	private Socket s;
	private Handler uiHandler;
	private Semaphore semaphore;
	private CameraImageHolder imageHolder;
	private HandlerHolder cameraHandlerHolder;
	private boolean motion = false;

	public ServerClient(Socket s, Handler uiHandler, Semaphore semaphore,
						CameraImageHolder imageHolder, HandlerHolder cameraHandlerHolder) {
		this.s = s;
		this.uiHandler = uiHandler;
		this.semaphore = semaphore;
		this.imageHolder = imageHolder;
		this.cameraHandlerHolder = cameraHandlerHolder;
	}

	@Nullable
	private String getContentType(String fileName) {
		File sdRoot = Environment.getExternalStorageDirectory();

		String filePath = sdRoot.toString() + fileName;
		File file = new File(filePath);

		String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
//		Log.d("SERVER", "Extension is: " + extension);

		if (extension != null) {
			String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
//			Log.d("SERVER", "MIME type is: " + mime);
			return mime;
		}
		return null;
	}

	private byte[] readFile(String fileName) throws IOException {
		File sdRoot = Environment.getExternalStorageDirectory();

		String filePath = sdRoot.toString() + "/httpServer" + fileName;
//		Log.d("SERVER", "File path: " + filePath);
		File file = new File(filePath);
		int sizeOfFile = (int) file.length();

		byte[] fileContent = new byte[sizeOfFile];


		BufferedInputStream buff = new BufferedInputStream(new FileInputStream(file));
		buff.read(fileContent, 0, fileContent.length);
		buff.close();

		return fileContent;
	}

	private String convertToBase64(String fileName) {
		byte[] b = this.imageHolder.getImage();

		if (b == null) {
			Log.e("SERVER", "Image object is null - reading from SD card");
			File sdRoot = Environment.getExternalStorageDirectory();

			String filePath = sdRoot.toString() + "/httpServer" + fileName;
			Bitmap bmp = BitmapFactory.decodeFile(filePath);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			b = baos.toByteArray();
		}

		return Base64.encodeToString(b, Base64.DEFAULT);
	}

	private void renderPictureFrame() {
		try {
			OutputStream o = this.s.getOutputStream();
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(o));

			String encodeImage = "data:image/png;base64," + convertToBase64("/picture.jpg");

			out.write("HTTP/1.x 200 OK\n");
			out.write("Server: LiteSpeed\n");
			out.write("Content-Type: text/html; charset=UTF-8\n");
			out.write("\n");

			out.write(" <!DOCTYPE html>\n" +
					"<html>\n" +
					"<head><meta http-equiv=\"refresh\" content=\"3\"></head>\n" +
					"<body>\n" +
					"<img src='" + encodeImage +
					"'> </img>\n" +
					"</body>\n" +
					"</html>\n");

			out.flush();

			sendMessageToUi(generateMsg("picture.html", "text/html"));
//			Log.d("SERVER", "Send autorefresh picture frame");

		} catch (IOException e) {

			Log.e("SERVER", "Error send autorefresh picture frame");
		}
	}

	private void sendMessageToUi(Bundle bundle) {
		Message msg = new Message();
		msg.setData(bundle);
		this.uiHandler.sendMessage(msg);
	}

	private Bundle generateMsg(String file, String type) {
		// create message bundle
		Bundle bundle = new Bundle();
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
		bundle.putString("time", dateFormat.format(new Date()));
		bundle.putString("client", s.getInetAddress().toString().replace("/", ""));
		bundle.putString("file", file.replace("/", ""));
		bundle.putString("type", type);

		return bundle;
	}

	private void sendImageMotion(byte[] picture) {
		OutputStream o = null;
		try {
			o = this.s.getOutputStream();
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(o));

			out.write("--boundary\n");
			out.write("Content-Type: image/jpg\n");
			out.write("Content-Length: " + picture.length + "\n");
			out.write("\n");
			out.flush();

			o.write(picture);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@NonNull
	private String getError404Page() {
		return "HTTP/1.x 404 OK\n" +
				"Server: Limsg.append()" +
				"Content-Type: text/html; charset=UTF-8\n" +
				"\n" +
				"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" +
				"<h1>File not found</h1>\n";
	}

	private String getHeader(String contentType) {
		StringBuilder out = new StringBuilder();
		out.append("HTTP/1.x 200 OK\n");
		out.append("Server: LiteSpeed\n");
		out.append("Content-Type: " + contentType + "; charset=UTF-8\n");
		out.append("\n");

		return out.toString();
	}

	public void run() {

		Handler cameraHandler = new Handler(Looper.getMainLooper()) {
			@Override
			public void handleMessage(Message msg) {

//				Log.d("SERVER", "Camera Handler - incoming data");

				if (motion) {
					Bundle bundle = msg.getData();
					sendImageMotion(bundle.getByteArray("data"));
				}
			}
		};

		this.cameraHandlerHolder.addHandler(cameraHandler);

		try {
			OutputStream o = this.s.getOutputStream();

			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(o));
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

			String url = "";
			String firstLine = in.readLine();
			if (firstLine != null && !firstLine.isEmpty()) {
				Log.d("SERVER", firstLine);
				String[] parts = firstLine.split(" ");
//				Log.d("Server", parts[1]);
				url = parts[1];
			}

			// rewrite url
			if (Pattern.matches("/", url)) {  // match empty address to index file
				url = "/index.html";
			}

			// match patterns in URL
			if (Pattern.matches("/picture.html", url)) { // match refresh frame for camera picture
				renderPictureFrame();
//				s.close();
//				semaphore.release();
//				cameraHandlerHolder.removeHandler(cameraHandler);
//				return;
			} else if (Pattern.matches("/camera", url)) { // match motion jpeg
				motion = true;

				out.write("HTTP/1.x 200 OK\n");
				out.write("Server: LiteSpeed\n");
				out.write("Cache-Control: no-cache\n");
				out.write("Cache-Control: private\n");
				out.write("Content-Type: multipart/x-mixed-replace;boundary=--boundary\n");
				out.write("\n");
				out.flush();

				try {
					while (true) {
						sleep(30);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else if (Pattern.matches("/cgi-bin/.+", url)) { // match cgi script
				String cmd = url.substring(url.indexOf("/", 1) + 1);
				cmd = cmd.replace("%20", " ");
				Log.d("SERVER", cmd);

				ShellAdapter sh = new ShellAdapter();
				String cmdOutput = sh.execCmd(cmd);

				out.write(getHeader("text/html"));
				out.write("<body bgcolor=\"#000\" text=\"#55ea27\"><code>");
				out.write("<p>" + cmdOutput + "</p>");
				out.write("</code></body>");
				out.flush();


			} else { // match standard url

				try {
					String contentType = getContentType(url);
					if (contentType == null) {
						throw new IOException();
					}

					byte[] results = readFile(url);
					out.write(this.getHeader(contentType));
					out.flush();

					for (byte i : results) {
						o.write(i);
					}

					Log.d("SERVER", "File sent");

					// send connection information to UI
					sendMessageToUi(generateMsg(url.replace("/", ""), contentType));

				} catch (IOException e) {
					out.write(getError404Page());
					out.flush();
					Log.d("SERVER", "File not found");
				}
			}

			s.close();

		} catch (IOException e) {
			Log.e("SERVER", "Socket error");
			e.printStackTrace();
		} finally {
			// release semaphore resources
			this.semaphore.release();
			this.cameraHandlerHolder.removeHandler(cameraHandler);
		}
	}
}
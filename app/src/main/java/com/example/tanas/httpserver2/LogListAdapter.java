package com.example.tanas.httpserver2;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tanas on 26.02.2017.
 */

public class LogListAdapter extends ArrayAdapter<Bundle> {

	private Context context;
	private int layoutResourceId;
	private List<Bundle> bundles;

	public LogListAdapter(Context context, int resource, List<Bundle> objects) {
		super(context, resource, objects);

		this.context = context;
		this.layoutResourceId = resource;
		this.bundles = objects;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		EntryHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new EntryHolder();
			holder.time = (TextView)row.findViewById(R.id.textViewTime);
			holder.clientIp = (TextView)row.findViewById(R.id.textViewClientIp);
			holder.requestUrl = (TextView)row.findViewById(R.id.textViewRequestUrl);
			holder.contentType = (TextView)row.findViewById(R.id.textViewContentType);

			row.setTag(holder);
		}
		else {
			holder = (EntryHolder)row.getTag();
		}

		Bundle bundle = bundles.get(position);
		holder.time.setText(bundle.getString("time"));
		holder.clientIp.setText(bundle.getString("client"));
		holder.requestUrl.setText(bundle.getString("file"));
		holder.contentType.setText(bundle.getString("type"));

		return row;
	}

	private static class EntryHolder {
		TextView time;
		TextView clientIp;
		TextView requestUrl;
		TextView contentType;
	}

}

package com.example.tanas.httpserver2;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class HttpServerActivity extends AppCompatActivity implements View.OnClickListener {

	private SocketServer s;
	private static final int READ_EXTERNAL_STORAGE = 1;
	private static final int CAMERA = 2;

	private LogListAdapter logListAdapter;
	private ArrayList<Bundle> logList = new ArrayList<>();

	private Camera camera;

	private CameraImageHolder imageHolder;
	private HandlerHolder handlerHolder;

	private SocketServerService serverService;
	private boolean isServiceBound = false;

	private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			Log.d("SERVER", "camera callback");

			if (data != null && imageHolder != null && handlerHolder != null) {
				// set image holder
				imageHolder.setImage(data);
				// call message handlers in all threads
				handlerHolder.sendDataToAllHanders(data);

				Log.d("SERVER", "new picture taken");

				File sdRoot = Environment.getExternalStorageDirectory();
				String filePath = sdRoot.toString() + "/httpServer/picture.jpg";
				File image = new File(filePath);

				// write picture to SD card if picture does not exist
				if (!image.exists()) {
					try {
						FileOutputStream output = new FileOutputStream(image);
						output.write(data);
						output.close();
					} catch (FileNotFoundException e) {
						Log.e("SERVER", "Cant save image - bad filename");
					} catch (IOException e) {
						Log.e("SERVER", "Cant write to file");
					}
				}
			}
		}
	};


	// Service connection
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			SocketServerService.SocketServerServiceBinder binder = (SocketServerService.SocketServerServiceBinder) service;
			serverService = binder.getService();
			Log.d("SERVER", "Service connected");
			isServiceBound = true;

			// obtain camera holders
			imageHolder = serverService.getImageHolder();
			handlerHolder = serverService.getHandlerHolder();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
//			serverService = null;
			isServiceBound = false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_http_server);

		// camera callback
		if (this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			Log.d("SERVER", "Camera active");
		} else {
			Log.d("SERVER", "camera error");
		}

		// camera ability
		camera = getCameraInstance();
//		imageHolder = new CameraImageHolder();
//		handlerHolder = new HandlerHolder();

		Button btn1 = (Button) findViewById(R.id.button1);
		Button btn2 = (Button) findViewById(R.id.button2);
		Button btnPicture = (Button) findViewById(R.id.buttonPicture);

		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btnPicture.setOnClickListener(this);

		// init log list view
		Bundle b1 = new Bundle();
		b1.putString("time", "Time");
		b1.putString("client", "Client IP");
		b1.putString("type", "File");
		b1.putString("file", "Content type");
		logList.add(b1);

		logListAdapter = new LogListAdapter(this, R.layout.list_item, logList);
		ListView listView = (ListView) findViewById(R.id.logListView);
		listView.setAdapter(logListAdapter);

	}

	@Override
	protected void onStart() {
		super.onStart();

		// binding service
		Intent serviceIntent = new Intent(this, SocketServerService.class);
		Bundle b = new Bundle();

		ResultReceiver resultReceiver = new ServiceResultReciever(null); // results for log list
		serviceIntent.putExtra("results", resultReceiver);

		bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
		startService(serviceIntent);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// unbind service
		if (isServiceBound) {
			unbindService(mConnection);
			isServiceBound = false;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Context context = getApplicationContext();
		TextView lable = (TextView) findViewById(R.id.textView2);

		// Start btn
		if (v.getId() == R.id.button1) {
//			s = new SocketServer(h, imageHolder, handlerHolder);
//			s.start();

			WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
			String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

			Toast.makeText(context, "Server start", Toast.LENGTH_LONG).show();
			lable.setText("RUNNING at " + ip + ":12345");



		}
		// stop btn
		if (v.getId() == R.id.button2) {
			s.close();
			try {
				s.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			Toast.makeText(context, "Server stop", Toast.LENGTH_LONG).show();
			lable.setText("NOT RUNNING");
		}
		// camera btn
		if (v.getId() == R.id.buttonPicture) {
			Log.d("SERVER", "Take picture button pushed");
			camera.takePicture(null, mPicture, mPicture);
			checkCameraPermition();

		}
	}

	private Camera getCameraInstance() {
		Camera cam = null;
		if (checkCameraHardware()) {
			try {
				cam = Camera.open();
				Log.d("SERVER", "Camera instance successfully obtained");
			} catch (Exception e) {
				Log.e("SERVER", "Camera open fail");
			}
		} else {
			Log.e("SERVER", "There is no camera");
		}
		return cam;
	}

	private boolean checkCameraHardware() {
		return this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	private void checkCameraPermition() {
		int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
		Log.d("SERVER", "Camera permission: " + String.valueOf(permissionCheck));
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		switch (requestCode) {

			case READ_EXTERNAL_STORAGE:
				if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
//					s = new SocketServer(h, imageHolder, handlerHolder);
//					s.start();
				}
				break;

			case CAMERA:
				if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
					Log.d("SERVER", "camera permission");
					CameraHandler cameraHandler = new CameraHandler();
					Log.d("SERVER", "cam permission");
				}
				break;

			default:
				break;
		}
	}

	// handle results for log list
	private class ServiceResultReciever extends ResultReceiver {

		public ServiceResultReciever(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			String serverStatus = resultData.getString("serverStatus");
			if (serverStatus != null) {
				TextView lable = (TextView) findViewById(R.id.textView2);
				lable.setText(serverStatus);
			}

			logList.add(resultData);
			logListAdapter.notifyDataSetChanged();
		}
	}
}

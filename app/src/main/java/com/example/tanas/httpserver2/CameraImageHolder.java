package com.example.tanas.httpserver2;

import java.util.concurrent.Semaphore;

public class CameraImageHolder {
	private byte[] image = null;
	private Semaphore mutex;

	public CameraImageHolder() {
		mutex = new Semaphore(1);
	}

	public void setImage(byte[] image) {
		try {
			mutex.acquire();
			this.image = image;
			mutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public byte[] getImage() {
		byte[] result = null;
		try {
			mutex.acquire();
			result = this.image;
			mutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}
}

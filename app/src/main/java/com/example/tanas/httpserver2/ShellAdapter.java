package com.example.tanas.httpserver2;

import android.text.Html;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ShellAdapter {

	public String execCmd(String cmd) {

		StringBuilder output = new StringBuilder();

		Process p;
		try {
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();

			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = input.readLine()) != null) {
//				Log.d("SERVER", "Cmd: " + line);
				output.append(Html.escapeHtml(line));
				output.append("<br />");
			}

		} catch (IOException e) {
			Log.e("SERVER", "Invalid cmd");
			return "Permission denied";

		} catch (InterruptedException e) {
			Log.e("SERVER", "Response timeout");
		}

		return output.toString();
	}

	public void testCmd() {
		Process p;
		try {
			p = Runtime.getRuntime().exec("pwd");

			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = input.readLine();
			Log.d("SERVER", line);

		} catch (IOException e) {
			e.printStackTrace();
			Log.e("SERVER", e.getMessage());
		}
	}

}
